//
//  CNSSlideableView.m
//  CNSSliderTest
//
//  Created by Christian Netthöfel on 05.03.14.
//  Copyright (c) 2014 adesso mobile solutions GmbH. All rights reserved.
//

#import "CNSSlideableView.h"

@interface CNSSlideableView ()

@end

@implementation CNSSlideableView

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    // Initialization code
    self.userInteractionEnabled = YES;
  }
  return self;
}

@end
