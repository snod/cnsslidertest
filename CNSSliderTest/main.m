//
//  main.m
//  CNSSliderTest
//
//  Created by Christian Netthöfel on 05.03.14.
//  Copyright (c) 2014 adesso mobile solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CNSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CNSAppDelegate class]));
    }
}
