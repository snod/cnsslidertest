//
//  CNSViewController.m
//  CNSSliderTest
//
//  Created by Christian Netthöfel on 05.03.14.
//  Copyright (c) 2014 adesso mobile solutions GmbH. All rights reserved.
//

#import "CNSViewController.h"

#import "CNSSlideableView.h"

@interface
CNSViewController () <UICollisionBehaviorDelegate, UIDynamicAnimatorDelegate>

@property(strong, nonatomic)
    UICollisionBehavior *collisionBehavior; // collision behavior
@property(strong, nonatomic)
    UIDynamicItemBehavior *dynamicItemBehavior; // dynamic item behavior
@property(strong, nonatomic)
    UIGravityBehavior *gravityBehavior; // gravity behavior
@property(strong, nonatomic)
    UIDynamicAnimator *dynamicAnimator;                    // dynamic animator
@property(strong, nonatomic) UIPushBehavior *pushBehavior; // push animator

@property CGFloat minAllowedCenterXCoord;
@property CGFloat maxAllowedCenterXCoord;

@property(nonatomic, strong) CNSSlideableView *slideView;

@end

@implementation CNSViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view, typically from a nib.
  self.slideView = [[CNSSlideableView alloc]
      initWithFrame:CGRectMake(0.0f, 50.0f, 100.0f, 500.0f)];
  self.slideView.backgroundColor = [UIColor redColor];

  [self.view addSubview:self.slideView];

  UIGestureRecognizer *panGestureRecognizer =
      [[UIPanGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(panRecognized:)];
  [self.slideView addGestureRecognizer:panGestureRecognizer];

  self.dynamicAnimator =
      [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
  self.collisionBehavior =
      [[UICollisionBehavior alloc] initWithItems:@[ self.slideView ]];
  self.gravityBehavior =
      [[UIGravityBehavior alloc] initWithItems:@[ self.slideView ]];
  self.dynamicItemBehavior =
      [[UIDynamicItemBehavior alloc] initWithItems:@[ self.slideView ]];
  self.pushBehavior =
      [[UIPushBehavior alloc] initWithItems:@[ self.slideView ]
                                       mode:UIPushBehaviorModeContinuous];

  [self.collisionBehavior
      addBoundaryWithIdentifier:@"left"
                      fromPoint:CGPointMake(-1.0f, 0.0f)
                        toPoint:CGPointMake(-1.0f,
                                            CGRectGetMaxY(self.view.frame))];

  [self.collisionBehavior
      addBoundaryWithIdentifier:@"right"
                      fromPoint:CGPointMake(
                                    CGRectGetMaxX(self.view.frame) + 1.0f, 0)
                        toPoint:CGPointMake(CGRectGetMaxX(self.view.frame) +
                                                1.0f,
                                            CGRectGetMaxY(self.view.frame))];

  self.collisionBehavior.collisionDelegate = self;

  self.gravityBehavior.gravityDirection = CGVectorMake(-1.0f, .0f);
  self.dynamicItemBehavior.elasticity = 0.0f;

  [self.dynamicAnimator addBehavior:self.collisionBehavior];
  [self.dynamicAnimator addBehavior:self.gravityBehavior];
  [self.dynamicAnimator addBehavior:self.pushBehavior];
  [self.dynamicAnimator addBehavior:self.dynamicItemBehavior];
  self.dynamicAnimator.delegate = self;
  self.minAllowedCenterXCoord = self.slideView.frame.size.width / 2;
  self.maxAllowedCenterXCoord =
      CGRectGetMaxX(self.view.frame) - self.slideView.frame.size.width / 2;
}

- (void)panRecognized:(UIPanGestureRecognizer *)gestureRecognizer {
  UIView *view = gestureRecognizer.view;
  CGPoint location = [gestureRecognizer locationInView:[view superview]];
  CGPoint translation = [gestureRecognizer translationInView:view];
  CGFloat xVelocity = [gestureRecognizer velocityInView:view].x;

  if ((xVelocity <= .0f &&
       view.center.x + translation.x <= self.minAllowedCenterXCoord) ||
      (xVelocity >= .0f &&
       view.center.x + translation.x >= self.maxAllowedCenterXCoord)) {
    return;
  }

  view.center =
      CGPointMake(view.center.x + translation.x, ceilf(view.center.y));

  [gestureRecognizer setTranslation:CGPointZero inView:view];

  NSLog(@"moved center %@", NSStringFromCGPoint(view.center));

  if (gestureRecognizer.state == UIGestureRecognizerStateEnded ||
      gestureRecognizer.state == UIGestureRecognizerStateCancelled) {

    //    [view setUserInteractionEnabled:NO];

    // optimize xVelocity

    // between -1.0 and 1.0, depending of the distance to the left or right
    // boundary
    float gravityMulitplicator = 0.0f;

    float distanceToNearestBoundary = 0.0f;

    // should fall to the nearest boundary
    if (self.maxAllowedCenterXCoord - view.center.x >
        (self.maxAllowedCenterXCoord - self.minAllowedCenterXCoord) / 2) {
      distanceToNearestBoundary = self.maxAllowedCenterXCoord - view.center.x;
      gravityMulitplicator =
          -distanceToNearestBoundary /
          (self.maxAllowedCenterXCoord - self.minAllowedCenterXCoord) / 2;
    } else {
      distanceToNearestBoundary = view.center.x - self.minAllowedCenterXCoord;
      gravityMulitplicator =
          distanceToNearestBoundary /
          (self.maxAllowedCenterXCoord - self.minAllowedCenterXCoord) / 2;
    }

    // NSLog(@"GravityMultiplicator: %f", gravityMulitplicator);

    // it is a drag gesture
    if (xVelocity < 800.0 && xVelocity > -800.0) {
      [_gravityBehavior
          setGravityDirection:CGVectorMake(gravityMulitplicator, 0.f)];
      [_dynamicItemBehavior setElasticity:0.0 * gravityMulitplicator];
      [_pushBehavior
          setPushDirection:CGVectorMake(500 * gravityMulitplicator, 0)];

    } else {
      // it is a swipe gesture
      if (xVelocity > 0.0f) {
        gravityMulitplicator = 1.0f;
      } else {
        gravityMulitplicator = -1.0f;
      }

      [_gravityBehavior
          setGravityDirection:CGVectorMake(gravityMulitplicator, 0.f)];
      [_dynamicItemBehavior setElasticity:0.0];
      [_pushBehavior
          setPushDirection:CGVectorMake(800 * gravityMulitplicator, 0)];
    }

    [self.dynamicAnimator updateItemUsingCurrentState:view];

  } else if (gestureRecognizer.state == UIGestureRecognizerStateFailed) {
// should fall to the next boundary
#warning TODO when did the recognizer fail?
    NSLog(@"Swipe or Drag failed!");
  }
}

- (void)collisionBehavior:(UICollisionBehavior *)behavior
       beganContactForItem:(id<UIDynamicItem>)item
    withBoundaryIdentifier:(id<NSCopying>)identifier
                   atPoint:(CGPoint)p {

  // Turn user interaction back on
  NSLog(@"lock to boundary");
  [self.slideView setUserInteractionEnabled:YES];
}

- (void)dynamicAnimatorDidPause:(UIDynamicAnimator *)animator {
  NSLog(@"dynamic animatator did pause");
}
@end
